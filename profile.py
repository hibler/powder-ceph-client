"""This profile creates a node and attaches it to the shared vlan for the
   Powder Ceph cluster.

Instructions:
Choose one or more nodes of a specific type to have access to the project's
Ceph filesystem. This will attach the nodes to the Ceph shared VLAN,
automatically assigning IP addresses. The nodes themselves are placed into
a LAN independent of the Ceph LAN.
"""

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Emulab extension
import geni.rspec.emulab

# Create a portal context.
pc = portal.Context()

pc.defineParameter("node_type", "Powder Node Type for client",
                   portal.ParameterType.STRING, "d430",
                   (
                    ("d710", "Emulab d710: 1x4-core 2.4GHz, 8GB RAM, 1Gbps Ethernet"),
                    ("d430", "Emulab d430: 2x8-core 2.4GHz, 64GB RAM, 10Gbps Ethernet"),
                    ("d820", "Emulab d820: 4x8-core 2.2GHz, 128GB RAM, 10Gbps Ethernet"),
                    ("d740", "Powder d740: 2x12-core 2.6GHz, 96GB RAM, 10Gbps Ethernet"),
                    ("d840", "Powder d840: 4x16-core 2.1GHz, 768GB RAM, 40Gbps Ethernet"),
                   ),
                   longDescription="Powder client node type"
                  )
pc.defineParameter("num_nodes", "Number of nodes of this type",
                   portal.ParameterType.INTEGER, 1,
                   longDescription="Number of nodes of this type to allocate."
                  )
pc.defineParameter("dataset", "URN of dataset to map to node",
                   portal.ParameterType.STRING, "",
                   longDescription="URN of a remote dataset to map RW to the first node in the experiment."
                  )
pc.defineParameter("rwdataset", "Map dataset read-write",
                   portal.ParameterType.BOOLEAN, False,
                   longDescription="Map dataset allowing modifications."
                  )
pc.defineParameter("nodelan", "Put nodes in an experiment LAN",
                   portal.ParameterType.BOOLEAN, False,
                   longDescription="True if nodes should be in a LAN other than the shared Ceph LAN."
                  )
pc.defineParameter("cephlan", "DEBUG: explicitly put nodes in Ceph LAN",
                   portal.ParameterType.BOOLEAN, False,
                   longDescription="True if profile should directly put nodes in the Ceph LAN. For debugging."
                  )


params = pc.bindParameters()
pc.verifyParameters()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

if params.cephlan:
    cephlan = request.LAN("cephlan")

if params.nodelan and params.num_nodes > 1:
    nodelan = request.LAN("lan")
    pass

for i in range(0, params.num_nodes):
    node = request.RawPC("node%d" % i)
    node.hardware_type = params.node_type
    # XXX only works with Ubuntu22 right now.
    node.disk_image = 'urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD'
    if params.cephlan:
        iface = node.addInterface()
        if params.node_type == "d710":
            iface.bandwidth = 1 * 1000 * 1000
            pass
        elif params.node_type == "d840":
            iface.bandwidth = 40 * 1000 * 1000
            pass
        else:
            iface.bandwidth = 10 * 1000 * 1000
            pass
        cephlan.addInterface(iface)
        node.addService(pg.Execute(shell="sh", command="sudo bash /local/repository/scripts/startup.sh"))
        pass
    else:
        node.mountCephFS()
        pass
    if params.nodelan and params.num_nodes > 1:
        nodelan.addInterface(node.addInterface())
        pass
    if i == 0 and params.dataset != "":
        iface = node.addInterface()

        fsnode = request.RemoteBlockstore("fsnode", "/mydataset")
        fsnode.dataset = params.dataset
        fslink = request.Link("fslink")
        fslink.addInterface(iface)
        fslink.addInterface(fsnode.interface)
        fslink.best_effort = True
        fslink.vlan_tagging = True
        if not params.rwdataset:
            fsnode.readonly = True
        pass
    pass

if params.cephlan:
    cephlan.best_effort = True
    cephlan.link_multiplexing = True
    if params.node_type != "d710":
        cephlan.setJumboFrames();
        pass
    cephlan.connectSharedVlan("powder-ceph");
    pass

if params.nodelan and params.num_nodes > 1:
    nodelan.best_effort = True
    #nodelan.link_multiplexing = True
    pass

# Print the RSpec to the enclosing page.
pc.printRequestRSpec(request)
