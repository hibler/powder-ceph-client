This is a git-based profile for setting up a single node that will be
connected to the Powder Ceph storage cluster. This cluster has about 1PB
of usable storage and is currently exposed as a shared filesystem.

### What you will see

Once instantiated, you should have a `/cephfs` filesystem. If you do
`df /cephfs` it should show something like:
```
Filesystem                                                                 1K-blocks        Used     Available Use% Mounted on
192.168.152.1:6789,192.168.152.2:6789,192.168.152.3:6789:/proj/testbed 1413840859136 12727398400 1401113460736   1% /cephfs
```
If instead it shows something like:
```
Filesystem     1K-blocks    Used Available Use% Mounted on
/dev/sda3       65478604 3071056  59052108   5% /
```
then the setup failed for some reason.

Assuming everything setup correctly, you should be able to read/write files
as you would in any POSIX-like filesystem. You should also be able to perform
RADOS based actions as long as you correctly specify the client name and
namespace. For example, to do a RADOS benchmark:
```
rados bench -p cephfs_data -n client._proj_ -N _proj_ 10 write 
```
where _proj_ is the name of your project (e.g., "testbed"). This example
will perform a write test for 10 seconds and give you a feel for the
throughput possible to the object stores.