#!/bin/bash

CEPH_REL=quincy
OS_REL=$(lsb_release -sc)
PROJ=`cat /var/emulab/boot/nickname | awk 'BEGIN {FS="."};{print $3}'`

# Sanity checks
if [ $SHELL != "/bin/bash" -o $UID -ne 0 ]; then
    echo "Startup must be run as root (not $UID) in bash (not $SHELL)"
    exit 1
fi
if [ -z "$PROJ" -o ! -d /proj/$PROJ/ ]; then
    echo "/proj/$PROJ not mounted!?"
fi

FSID=""
MON=""
KEY=""

# XXX fixed for now, get from tmcd?
FSID=3bed7d95-30d2-4d66-b3bb-e57a41c9b460
MON=192.168.152.1,192.168.152.2,192.168.152.3

# XXX fernow hack to get the project-specific secret
if [ -f /proj/$PROJ/ceph/secret ]; then
    KEY=`cat /proj/$PROJ/ceph/secret`
else
    echo "No secret file in /proj/$PROJ/ceph!"
fi

if [ -z "$FSID" -o -z "$MON" -o -z "$KEY" ]; then
    echo "Cannot get necessary Ceph info"
    exit 1
fi

# install Ceph packages
# XXX we used to install using the Ceph instructions, downloading their
# key and using the appropriate release repo. But that breaks, apparently
# due to a bad IP address in the server list of download.cpeh.com.
# So let's just install whatever Ubuntu says to install.
if [ ! -x /usr/bin/ceph ]; then
    sudo apt-get update
    sudo apt-get install -y ceph-common attr
fi

# setup /etc/ceph
if [ ! -f /etc/ceph/ceph.conf ]; then
    mkdir -p /etc/ceph
    cat >/etc/ceph/ceph.conf <<EOF
[global]
  fsid = $FSID
  mon_host = $MON
EOF
    chmod 644 /etc/ceph/ceph.conf
fi
if [ ! -f /etc/ceph/ceph.client.$PROJ.keyring ]; then
    cat >/etc/ceph/ceph.client.$PROJ.keyring <<EOF
[client.$PROJ]
        key = $KEY
EOF
    chown root:$PROJ /etc/ceph/ceph.client.$PROJ.keyring
    chmod 640 /etc/ceph/ceph.client.$PROJ.keyring
    # XXX so ceph commands don't complain about "no keyring file"
    ln -fn /etc/ceph/ceph.client.$PROJ.keyring /etc/ceph/ceph.keyring
fi

# mount the filesystem
if ! grep -q "^$PROJ@.cephfs" /etc/fstab; then
    MONARG="mon_addr=${MON//,/\/}"
    echo "$PROJ@.cephfs=/proj/$PROJ /cephfs ceph $MONARG,noatime 0 0" | \
        sudo sh -c 'cat >> /etc/fstab'
fi
if ! mount | grep -q /cephfs; then
    mkdir -p /cephfs
    sudo mount /cephfs
fi

exit 0
